//エラー回避
#define _CRT_SECURE_NO_WARNINGS


//インクルード
#include "FileWriter.h"
#include <ctime>
#include <filesystem>
#include <fstream>
#include <Windows.h>


//ファイル書き込み名前空間
namespace FileWriter
{
	//初期化フラグと解放フラグ
	bool				fileInitFlg = false;
	bool				fileReleFlg = false;

	//ファイル書き込み
	std::ofstream		logFile;

	//時間
	time_t				t = time(NULL);

	//時間取得
	std::string GetDate();
}


//初期化
void FileWriter::Initialize(std::string filePath)
{
	//既に初期化済み
	if (fileInitFlg)return;

	//フラグ切り替え
	fileInitFlg = true;

	//フォルダが存在するか確認し存在しない場合作成
	if (!std::experimental::filesystem::exists("./Log"))
		std::experimental::filesystem::create_directory("./Log");

	//ファイルを空にする
	logFile.open("Log\\" + filePath, std::ofstream::out | std::ofstream::trunc);
}

//書き込み
void FileWriter::Write(std::string message, bool getTime, bool separate)
{
	//区切り
	if(separate)
		logFile << "--------------------------\n";

	//ファイル書き込み
	logFile << message << std::endl;

	//時間記述
	if(getTime)
		logFile << "Written  time   :" << GetDate() << "\n";

	//区切り
	if (separate)
		logFile << "--------------------------\n";
}

//解放
void FileWriter::Release()
{
	//既に解放済み
	if (fileReleFlg)return;

	//フラグ切り替え
	fileReleFlg = true;

	//ファイル開放
	logFile.close();
}

//時間取得
std::string FileWriter::GetDate()
{
	//文字配列初期化
	char date[64];

	//時間設定
	strftime(date, sizeof(date), "%Y/%m/%d %a %H:%M:%S", localtime(&t));

	//文字列返却
	return date;
}