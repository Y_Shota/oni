//インクルードガード
#pragma once
#ifndef FILEWRITER_H
#define FILEWRITER_H
#endif // !FILEWRITER_H


//インクルード
#include <string>


//ディファイン
#define FW FileWriter


//ファイル書き込み名前空間
namespace FileWriter
{
	//初期化
	void Initialize(std::string filePath);

	//書き込み
	void Write(std::string message, bool getTime = false, bool separate = false);

	//解放
	void Release();
};