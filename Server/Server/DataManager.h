#pragma once
#include <memory>

#include "SessionPacket.h"


class PlayerManager;

//データの送受信を管理する
class DataManager {

private:
	int sessionSocket_;
	bool shouldQuit_;
	bool isStart_;
	float timer_;
	std::unique_ptr<PlayerManager> playerManager_;

public:
	DataManager();
	~DataManager();
	
	void Initialize();
	void Update();
	void Finalize();
	
	auto ShouldQuit() const -> bool;

private:
	void InitializePlayerManager();
	void FinalizePlayerManager();
	void ApplySessionPacket(const std::shared_ptr<SessionPacket>& packet);

	void ClearSessionSocket();
	
};

