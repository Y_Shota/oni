#include "PlayerState.h"

using namespace std;


PlayerState::PlayerState()
	: isStart_(false) {}

void PlayerState::Start() {

	isStart_ = true;
}

void PlayerState::End() {

	isStart_ = false;
}

auto PlayerState::IsStart() const -> bool {

	return isStart_;
}

auto PlayerState::SetPacket(shared_ptr<PlayerDataPacket> playerPacket) -> PlayerState* {

	playerPacket_ = playerPacket;
	return this;
}

auto PlayerState::GetPacket() const -> const shared_ptr<PlayerDataPacket>& {

	return playerPacket_;
}
