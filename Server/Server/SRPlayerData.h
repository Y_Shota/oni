//INCLUDE GUARD
#pragma once

//INCLUDE
#include <DirectXMath.h>

//受け取るプレイヤー情報の構造体
struct PLAYER_DATA {

	int id;						//識別番号
	int isDemon;				//鬼かどうか
	DirectX::XMVECTOR position;	//座標
	DirectX::XMVECTOR rotate;	//回転
	int nextDemonPlayerId;		//次の鬼がだれか
};