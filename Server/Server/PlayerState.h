#pragma once
#include "PlayerDataPacket.h"


class PlayerState final {

public:
	PlayerState();
	~PlayerState() = default;

	void Start();
	void End();
	auto IsStart() const -> bool;
	
	auto SetPacket(std::shared_ptr<PlayerDataPacket> playerPacket) -> PlayerState*;
	auto GetPacket() const -> const std::shared_ptr<PlayerDataPacket>&;

private:
	std::shared_ptr<PlayerDataPacket> playerPacket_;
	bool isStart_;
};

