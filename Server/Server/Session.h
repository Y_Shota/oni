#pragma once

enum SESSION_TYPE : int {

	SESSION_JOIN,		// C
	SESSION_JOINED,		// S
	SESSION_START,		// S/C
	SESSION_STARTED,	// C
	SESSION_END,		// S
	SESSION_ENDED,		// C
	SESSION_LEAVE,		// C
	SESSION_LEFT,		// S
	SESSION_CLOSE,		// C
	SESSION_CLOSED,		// S
	SESSION_MAX
};

struct SessionData {

	int id;				//プレイヤ識別番号　蔵への送信で負の場合不特定多数へ
	int sessionType;	//上記列挙格納
	long long option;	//その他情報
};