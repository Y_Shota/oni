#include "PlayerManager.h"

#include <iostream>


#include "PlayerState.h"
#include "Network/NetworkManager.h"

PlayerManager::PlayerManager()
	: playSocket_(-1), readyPlayers(0) {}

PlayerManager::~PlayerManager() = default;

void PlayerManager::Initialize() {

	playSocket_ = NetworkManager::CreateSocket(SOCKET_MODE::UDP_SERVER, 8000);
	NetworkManager::SetBlocking(playSocket_, false);
	NetworkManager::Unlock(playSocket_);
}

void PlayerManager::Update() {

	if (IsAllStart()) {

		auto receiveNum = NetworkManager::GetReceivePacketCount(playSocket_);
		for (auto i = 0; i < receiveNum; ++i) {

			auto receivePacket = NetworkManager::PopReceivePacket(playSocket_);
			if (auto playerDataPacket = std::dynamic_pointer_cast<PlayerDataPacket>(receivePacket)) {

				//送られてきたデータを参照
				const auto& playerData = playerDataPacket->GetRawData();

				//送り主のIDのプレイヤの状態を更新
				playerStateMap_.at(playerData.id)->SetPacket(playerDataPacket);
			}
		}

		//全プレイヤデータを全員に送信
		for (const auto&[id, playerState] : playerStateMap_) {

			NetworkManager::Send(playSocket_, playerState->GetPacket());
		}
	}
}

void PlayerManager::Finalize() {

	ClearPlaySocket();
	
	for (auto&[id, playerState] : playerStateMap_) {

		playerState.reset();
	}
	playerStateMap_.clear();
}

auto PlayerManager::GetPlayerCount() const -> int {

	return static_cast<int>(playerStateMap_.size());
}

auto PlayerManager::AddPlayer() -> int {

	static auto numberAssignedToUniqueId = 0;

	//ユニークなIDでプレイヤステートを作成いID付加用変数を
	playerStateMap_[numberAssignedToUniqueId] = std::make_unique<PlayerState>();

	std::cout << "Joined : " << numberAssignedToUniqueId << std::endl;
	
	return numberAssignedToUniqueId++;
}

void PlayerManager::RemovePlayer(int id) {

	auto result = playerStateMap_.find(id);

	if (result == playerStateMap_.end())	return;

	std::cout << "Left : " << id << std::endl;
	
	playerStateMap_.erase(result);
}

void PlayerManager::StartPlayer(int id) {

	try {

		playerStateMap_.at(id)->Start();
	}
	catch (std::out_of_range&) {}
}

void PlayerManager::EndPlayer(int id) {
	
	try {

		playerStateMap_.at(id)->End();
	}
	catch (std::out_of_range&) {}
}

void PlayerManager::CleanupReceiveData() const {

	auto receiveNum = NetworkManager::GetReceivePacketCount(playSocket_);
	for (auto i = 0; i < receiveNum; ++i) {

		auto receivePacket = NetworkManager::PopReceivePacket(playSocket_);
	}
}

auto PlayerManager::GetStartPlayerCount() const -> int {

	auto startPlayer = 0;
	for (const auto&[id, playerState] : playerStateMap_) {

		if (playerState->IsStart())	startPlayer++;
	}
	return startPlayer;
}

auto PlayerManager::IsAllStart() const -> bool {

	return playerStateMap_.size() == GetStartPlayerCount();
}

void PlayerManager::ClearPlaySocket() {

	if (playSocket_ != -1) {

		NetworkManager::RemoveSocket(playSocket_);
		playSocket_ = -1;
	}
}
