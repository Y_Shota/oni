#pragma once
#include "Network/PacketId.h"
#include "SRPlayerData.h"

class PlayerDataPacket : public PacketId<PLAYER_DATA> {

public:
	PLAYER_DATA ConvertToNetwork(PLAYER_DATA & host) override;
	PLAYER_DATA ConvertToHost(PLAYER_DATA & network) override;
};

