#include "DataManager.h"

#include <iostream>



#include "Network/NetworkManager.h"
#include "SessionPacket.h"
#include "PlayerDataPacket.h"
#include "PlayerManager.h"
#include "Time.h"

using namespace std;


DataManager::DataManager()
	: sessionSocket_(-1)
	, shouldQuit_(false)
	, isStart_(false)
	, timer_(0.f)
	, playerManager_(nullptr) {}

DataManager::~DataManager() = default;

void DataManager::Initialize() {

	//メッセージ用
	sessionSocket_ = NetworkManager::CreateSocket(SOCKET_MODE::UDP_SERVER, 8002);
	NetworkManager::SetBlocking(sessionSocket_, false);
	NetworkManager::Unlock(sessionSocket_);

	cout << NetworkManager::GetLocalHostName() << endl;
	cout << "open" << endl;
}

void DataManager::Update() {

	auto receiveNum = NetworkManager::GetReceivePacketCount(sessionSocket_);
	for (auto i = 0; i < receiveNum; ++i) {

		auto receivePacket = NetworkManager::PopReceivePacket(sessionSocket_);
		if (auto sessionPacket = dynamic_pointer_cast<SessionPacket>(receivePacket)) {

			//送られてきたデータを取得
			if (!playerManager_)	InitializePlayerManager();

			//情報解析適応
			ApplySessionPacket(sessionPacket);
		}
	}

	//プレイヤステートの更新
	if (isStart_ && playerManager_) {

		playerManager_->Update();

		timer_ += Time::GetFrameTimeMs() / 1000;
		if (timer_ >= 30.f) {

			cout << "End" << endl;
			
			auto packet = make_shared<SessionPacket>();
			SessionData data = {
				-1,
				SESSION_END,
				0
			};
			isStart_ = false;
			timer_ = 0.f;
			packet->SetRawData(data);
			NetworkManager::Send(sessionSocket_, packet);
		}
	}
}

void DataManager::Finalize() {

	ClearSessionSocket();
}

auto DataManager::ShouldQuit() const -> bool {

	return shouldQuit_;
}

void DataManager::ClearSessionSocket() {
	
	if (sessionSocket_ != -1) {

		NetworkManager::RemoveSocket(sessionSocket_);
		sessionSocket_ = -1;
	}
}

void DataManager::InitializePlayerManager() {

	//一応空っぽに
	FinalizePlayerManager();
	
	//プレイシーン用
	playerManager_ = make_unique<PlayerManager>();
	playerManager_->Initialize();
}

void DataManager::FinalizePlayerManager() {

	if (playerManager_) {
		
		playerManager_->Finalize();
	}
	playerManager_.reset();
}

void DataManager::ApplySessionPacket(const std::shared_ptr<SessionPacket>& packet) {

	const auto& sessionData = packet->GetRawData();
	SessionData data;
	switch (sessionData.sessionType) {

	//参加を要求された
	case SESSION_JOIN: {

		//未設定のプレイヤのみ受付
		if (sessionData.id != -1)	break;
		
		data = {
			playerManager_->AddPlayer(),	//プレイヤーID付加
			SESSION_JOINED,					//参加させたことを通知
			sessionData.option				//キーを付随
		};
		packet->SetRawData(data);
		NetworkManager::Send(sessionSocket_, packet);
		break;
	}

	//開始を要求された
	case SESSION_START: {

		cout << "Start" << endl;
		
		data = {
			sessionData.id,	//鬼指定
			SESSION_START,	//開始の合図
			0				//プレイヤ数を付随
		};
		isStart_ = true;
		playerManager_->CleanupReceiveData();
		packet->SetRawData(data);
		NetworkManager::Send(sessionSocket_, packet);
		break;
	}

	//開始完了した
	case SESSION_STARTED: {

		//開始したプレイヤーを記録
		playerManager_->StartPlayer(sessionData.id);
		break;
	}
		
	//終了を完了した
	case SESSION_ENDED: {

		playerManager_->EndPlayer(sessionData.id);
		break;
	}

	//退出要求
	case SESSION_LEAVE: {

		//プレイヤを排除
		playerManager_->RemovePlayer(sessionData.id);
		data = {
			sessionData.id,		//送信相手
			SESSION_LEFT,		//退出させた
			0	
		};
		packet->SetRawData(data);
		NetworkManager::Send(sessionSocket_, packet);
		break;
	}

	//サーバー閉鎖要求
	case SESSION_CLOSE: {
		data = {
			-1,				//全員
			SESSION_CLOSED,	//閉じた
			0
		};
		shouldQuit_ = true;
		packet->SetRawData(data);
		NetworkManager::Send(sessionSocket_, packet);
		break;
	}
		
	default: break;	//その他無視
	}
}
