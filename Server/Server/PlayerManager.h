#pragma once
#include <memory>
#include <unordered_map>

class PlayerDataPacket;
class SessionPacket;
class PlayerState;


class PlayerManager {

public:
	PlayerManager();
	~PlayerManager();
	
	void Initialize();
	void Update();
	void Finalize();

	auto GetPlayerCount() const -> int;

	auto AddPlayer() -> int;
	void RemovePlayer(int id);

	void StartPlayer(int id);
	void EndPlayer(int id);

	void CleanupReceiveData() const;

private:
	auto GetStartPlayerCount() const -> int;
	auto IsAllStart() const -> bool;
	void ClearPlaySocket();

private:
	using PlayerStatePtr = std::unique_ptr<PlayerState>;
	std::unordered_map<int, PlayerStatePtr> playerStateMap_;

	int playSocket_;
	int readyPlayers;
};

