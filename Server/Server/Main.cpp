#include "DataManager.h"
#include "Network/NetworkManager.h"
#include "Time.h"

using namespace std;

int main() {

	//初期化
	NetworkManager::Initialize();

	//鯖本体
	auto dataManager = make_unique<DataManager>();
	dataManager->Initialize();

	//メインループ
	for (;;) {

		//fps維持
		Time::CalculationSleep();

		//更新処理
		NetworkManager::Update();

		dataManager->Update();

		//終了命令が来ていたら終了
		if (dataManager->ShouldQuit())	break;
	}

	dataManager->Finalize();
	
	//終了処理
	NetworkManager::Finalize();
	
	return 0;
}