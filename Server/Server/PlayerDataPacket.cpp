#include "PlayerDataPacket.h"

PLAYER_DATA PlayerDataPacket::ConvertToNetwork(PLAYER_DATA & host) {

	PLAYER_DATA network;
	network.id = htonl(host.id);
	network.isDemon = htonl(host.isDemon);

	for (int i = 0; i < 4; i++) {

		network.position.m128_u32[i] = htonf(host.position.m128_f32[i]);
		network.rotate.m128_u32[i] = htonf(host.rotate.m128_f32[i]);
	}
	network.nextDemonPlayerId = htonl(host.nextDemonPlayerId);

	return network;
}

PLAYER_DATA PlayerDataPacket::ConvertToHost(PLAYER_DATA & network) {

	PLAYER_DATA host;
	host.id = ntohl(network.id);
	host.isDemon = ntohl(network.isDemon);

	for (int i = 0; i < 4; i++) {

		host.position.m128_f32[i] = ntohf(network.position.m128_u32[i]);
		host.rotate.m128_f32[i] = ntohf(network.rotate.m128_u32[i]);
	}
	host.nextDemonPlayerId = htonl(network.nextDemonPlayerId);

	return host;
}
