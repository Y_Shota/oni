#pragma once
#include "IPacket.h"


class IPacketId : public IPacket {

public:
	virtual ~IPacketId() = default;

	virtual auto GetId() -> int = 0;
};

