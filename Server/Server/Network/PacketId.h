#pragma once
#include <memory>

#include "IPacketId.h"
#include "NetworkException.h"

/**
 * \brief パケットをID管理するためのクラス
 * \tparam T 扱うデータ型
 * \tparam U = T 扱うデータ型のネットワーク用型　扱うデータ型のメンバが浮動小数を含むときに使用
 */
template<class T, class U = T>
class PacketId : public IPacketId {
	
public:

	using HostType = T;
	using NetworkType = U;
	
	PacketId() {

		data_ = std::make_unique<char[]>(sizeof(NetworkType) + sizeof(int));
	}

	static void SetId(int id) {

		id_ = id;
	}

	static auto SGetId() -> int {

		return id_;
	}
	
	auto GetId() -> int override {

		return SGetId();
	}

	void SetData(const char* data, u_long size) override {

		memcpy_s(data_.get(), size, data, size);
	}
	
	void SetRawData(HostType&& data) {

		if (id_ == -1)	throw NetworkException("PacketID is invalid number");
		
		rawData_ = std::make_unique<HostType>(data);
		auto size = sizeof(HostType);
		auto network = ConvertToNetwork(data);

		auto* id = reinterpret_cast<int*>(data_.get());
		*id = htonl(id_);

		memcpy_s(data_.get() + sizeof(int), size, &network, size);
	}

	void SetRawData(HostType& data) {

		if (id_ == -1)	throw NetworkException("PacketID is invalid number");

		rawData_ = std::make_unique<HostType>(data);
		auto size = sizeof(HostType);
		auto network = ConvertToNetwork(data);

		auto* id = reinterpret_cast<int*>(data_.get());
		*id = htonl(id_);

		memcpy_s(data_.get() + sizeof(int), size, &network, size);
	}

	auto GetRawData() -> const HostType& {

		if (!rawData_) {

			auto* networkData = reinterpret_cast<NetworkType*>(data_.get() + sizeof(int));
			rawData_ = std::make_unique<HostType>(ConvertToHost(*networkData));
		}

		return *rawData_.get();
	}
	
	auto GetSize() const -> u_long override {

		return sizeof(HostType) + sizeof(int);
	}
	
	auto GetData() const -> const char* override {

		return data_.get();
	}

	virtual NetworkType ConvertToNetwork(HostType& host) = 0;
	virtual HostType ConvertToHost(NetworkType& network) = 0;
	
private:
	std::unique_ptr<HostType> rawData_;
	std::unique_ptr<char[]> data_;
	static int id_;
};

template<class T, class U>
int PacketId<T, U>::id_ = -1;
