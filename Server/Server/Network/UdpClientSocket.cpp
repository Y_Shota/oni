#include "UdpClientSocket.h"

#include <WS2tcpip.h>

#include "NetworkException.h"


void UdpClientSocket::Open(u_short port) {

	CreateLocalSocket(port + 1);
	CreateRemoteSocket(port);
}

void UdpClientSocket::Connect(const char* destination) {

	if (isLock_)	return;
	
	destinationSet_.clear();
	
	u_long address;
	inet_pton(AF_INET, destination, &address);
	destinationSet_.emplace(address);
}

