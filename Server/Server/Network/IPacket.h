#pragma once

#include <WinSock2.h>


class IPacket {

public:
	virtual ~IPacket() = default;
	virtual void SetData(const char* data, u_long size) = 0;
	virtual auto GetSize() const -> u_long = 0;
	virtual auto GetData() const -> const char* = 0;
};