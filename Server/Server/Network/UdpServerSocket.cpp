#include "UdpServerSocket.h"

#include <WS2tcpip.h>

#include "NetworkException.h"


void UdpServerSocket::Open(u_short port) {

	CreateLocalSocket(port);
	CreateRemoteSocket(port + 1);
}

void UdpServerSocket::Connect(const char* destination) {

	if (isLock_)	return;
	
	u_long address;
	inet_pton(AF_INET, destination, &address);
	destinationSet_.emplace(address);
}

void UdpServerSocket::OtherProcess() {

	if (isLock_)	return;
	
	destinationSet_.merge(recipientSet_);
}

