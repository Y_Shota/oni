//インクルードガード
#pragma once
#ifndef PAYERS_H
#define PAYERS_H
#endif // !PAYERS_H


//インクルード
#include "Engine/GameObject.h"
#include "SRPlayerData.h"

//ディファイン
#define INIT_ID -1

class PlayersAdmin;

//プレイヤーを司るスーパークラス
class Players : public GameObject
{
protected:

	//モデルID
	int ID_[2];

	//鬼かどうか
	bool isOni_;

	//鬼が移った時のタイム
	int cantTime_;

	int playerId_;

public:
	//コンストラクタ
	Players(GameObject* parent, const std::string& name);

	//デストラクタ
	~Players();

	//初期化
	void Initialize() override = 0;

	//更新
	void Update() override = 0;

	//描画
	void Draw() override;

	//開放
	void Release() override = 0;

	//オブジェクトを固定
	void SetTransform(float x, float y, float z, float angY);

	//オブジェクトを固定
	void SetTransform(Transform trans) { transform_ = trans; }

	//鬼状態の設定
	void SetDemon(bool isOni);

	//鬼かどうかの取得
	auto IsDemon() const -> bool;

	//オブジェクトのトランスフォーム取得
	Transform GetTransform() const { return transform_; }

	//データを反映させる
	//params : プレイヤーデータ(構造体)
	//return : NULL
	virtual void ReflectData(const PLAYER_DATA& data);

	auto CanChangeDemon() const -> bool;

	void SetPlayerId(int id);
	auto GetPlayerId() const noexcept -> int;
};

