#pragma once
#include "Network/PacketId.h"
#include "Session.h"

class SessionPacket final : public PacketId<SessionData> {

public:
	NetworkType ConvertToNetwork(HostType& host) override;
	HostType ConvertToHost(NetworkType& network) override;
};

