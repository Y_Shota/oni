#define NOMINMAX
#include "SessionManager.h"

#include <random>


#include "IpAddress.h"
#include "Network/NetworkManager.h"
#include "SessionPacket.h"
#include "PlayersAdmin.h"
#include "Session.h"
#include "Engine/sceneManager.h"

using namespace std;


//コンストラクタ
SessionManager::SessionManager()
	: sessionSocket_(-1)
	, isConnected_(false)
	, myPlayerId_(-1)
	, firstDemon_(false)
	, keyNumber_(nullopt) {}

//デストラクタ
SessionManager::~SessionManager() = default;

//初期化
void SessionManager::Initialize()
{
	//メッセージ用
	sessionSocket_ = NetworkManager::CreateSocket(SOCKET_MODE::UDP_CLIENT, 8002);
	NetworkManager::SetBlocking(sessionSocket_, false);
	NetworkManager::Unlock(sessionSocket_);
}

//更新
void SessionManager::Update()
{
	
	auto receiveNum = NetworkManager::GetReceivePacketCount(sessionSocket_);
	for (auto i = 0; i < receiveNum; ++i) {
		
		auto receivePacket = NetworkManager::PopReceivePacket(sessionSocket_);
		if (auto sessionPacket = dynamic_pointer_cast<SessionPacket>(receivePacket)) {
			
			//送られてきたデータを取得
			//const auto& sessionData = sessionPacket->GetRawData();
			
			ApplySessionPacket(sessionPacket);
		}
	}
}

//開放
void SessionManager::Release()
{
	NetworkManager::RemoveSocket(sessionSocket_);
	sessionSocket_ = -1;
}

void SessionManager::SendJoinMessage() {

	//接続先を設定
	NetworkManager::Unlock(sessionSocket_);
	NetworkManager::Connect(sessionSocket_, IP_ADDRESS.data());
	NetworkManager::Lock(sessionSocket_);

	//キー生成用
	random_device seedGenerator;
	default_random_engine engine(seedGenerator());
	uniform_int_distribution<long long> distribute(
		numeric_limits<long long>::min(),
		numeric_limits<long long>::max());

	//キー設定
	keyNumber_ = distribute(engine);
	
	//パケットを作成し、データ設定する
	auto packet = make_shared<SessionPacket>();
	packet->SetRawData(SessionData{ myPlayerId_, SESSION_JOIN, keyNumber_.value() });

	//送信
	NetworkManager::Send(sessionSocket_, packet);
}

void SessionManager::SendLeaveMessage() const {

	//パケットを作成し、データ設定する
	auto packet = make_shared<SessionPacket>();
	packet->SetRawData(SessionData{ myPlayerId_, SESSION_LEAVE, 0 });

	//送信
	NetworkManager::Send(sessionSocket_, packet);
}

void SessionManager::SendStartMessage() const {

	//パケットを作成し、データ設定する
	auto packet = make_shared<SessionPacket>();
	packet->SetRawData(SessionData{ myPlayerId_, SESSION_START, 0 });

	//送信
	NetworkManager::Send(sessionSocket_, packet);
}

void SessionManager::SendStartedMessage() const {

	//パケットを作成し、データ設定する
	auto packet = make_shared<SessionPacket>();
	packet->SetRawData(SessionData{ myPlayerId_, SESSION_STARTED, 0 });

	//送信
	NetworkManager::Send(sessionSocket_, packet);
}

void SessionManager::SendEndedMessage() const {

	//パケットを作成し、データ設定する
	auto packet = make_shared<SessionPacket>();
	packet->SetRawData(SessionData{ myPlayerId_, SESSION_ENDED, 0 });

	//送信
	NetworkManager::Send(sessionSocket_, packet);
}

auto SessionManager::GetMyPlayerId() const -> int {

	return myPlayerId_;
}

auto SessionManager::IsFirstDemon() const -> bool {

	return firstDemon_;
}

void SessionManager::ApplySessionPacket(const std::shared_ptr<SessionPacket>& packet) {

	//送信されてきたデータを取得
	const auto& sessionData = packet->GetRawData();

	//メッセージ内容によって処理を変える
	switch (sessionData.sessionType) {

	//参加完了
	case SESSION_JOINED: {

		//既に参加済みなら無視
		if (isConnected_)	break;

		//キー未生成または作成したキーと異なるなら無視
		if (!keyNumber_.has_value())					break;
		if (keyNumber_.value() != sessionData.option)	break;

		//自身のIDを記憶
		myPlayerId_ = sessionData.id;
		
		isConnected_ = true;
		break;
	}

	//開始を要求された
	case SESSION_START: {

		if (myPlayerId_ == sessionData.id) {

			firstDemon_ = true;
		}
		SceneManager::GetInstance()->ChangeScene(SCENE_ID_PLAY);
		break;
	}

	//終了通知が来た
	case SESSION_END: {

		firstDemon_ = false;
		SceneManager::GetInstance()->ChangeScene(SCENE_ID_MATCH);
		SendEndedMessage();
		break;
	}

	//退出が完了した
	case SESSION_LEFT: {

		if (myPlayerId_ != sessionData.id)	break;
		
		//キーを無効化
		keyNumber_ = nullopt;
		myPlayerId_ = -1;
		//NetworkManager::RemoveSocket(sessionSocket_);
		isConnected_ = false;
		break;
	}

	//鯖が閉鎖した
	case SESSION_CLOSED: {

		isConnected_ = false;
		break;
	}
		
	default: break;
	}
}

unique_ptr<SessionManager> SessionManager::instance_;

auto SessionManager::GetInstance() -> SessionManager* {

	Create();
	return instance_.get();
}

void SessionManager::Create() {

	if (!instance_) {

		struct A : SessionManager {};
		instance_ = make_unique<A>();
		instance_->Initialize();
	}
}

void SessionManager::Destroy() {

	instance_.reset();
}
