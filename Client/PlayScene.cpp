
#include "SessionManager.h"
/*エンジン*/
/*親*/
#include "PlayScene.h"
/*子供*/
#include "PlayersAdmin.h"
#include "Stage.h"
/*相互*/

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	//セッションマネージャを取得
	auto* sessionManager = SessionManager::GetInstance();

	
	//Stageの生成
	Instantiate<Stage>(this);

	Instantiate<PlayersAdmin>(this);


	
	//開始完了したことを送信
	sessionManager->SendStartedMessage();
}

//更新
void PlayScene::Update()
{
	auto* sessionManager = SessionManager::GetInstance();
	sessionManager->Update();
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}