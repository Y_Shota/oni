//INCLUDE
#include "PlayersAdmin.h"

#include "Enemy.h"
#include "IpAddress.h"
#include "Network/NetworkManager.h"
#include "PlayerDataPacket.h"
#include "Players.h"
#include "SessionManager.h"
#include "You.h"


PlayersAdmin::PlayersAdmin(GameObject* parent) 
	: GameObject(parent, "PlayersAdmin")
	, socketID_(-1), myself(nullptr) {

	//プレイヤーリストクリア
	pPlayerMap_.clear();
}

PlayersAdmin::~PlayersAdmin() {

}

void PlayersAdmin::Initialize() {

	//作成されたソケットIDを取得
	socketID_ = NetworkManager::CreateSocket(SOCKET_MODE::UDP_CLIENT, 8000);
	NetworkManager::SetBlocking(socketID_, false);
	NetworkManager::Connect(socketID_, IP_ADDRESS.data());
	
	//操作プレイヤー
	myself = Instantiate<You>(this);

	auto* sessionManager = SessionManager::GetInstance();

	auto myPlayerId = sessionManager->GetMyPlayerId();
	
	AddPlayer(myPlayerId, myself);
	myself->SetDemon(sessionManager->IsFirstDemon());
	
	myDataPacket_ = std::make_unique<PlayerDataPacket>();
}

void PlayersAdmin::Update() {

	//データの受信
	ReceiveData();

	SendData();
}

void PlayersAdmin::Draw() {

}

void PlayersAdmin::Release() {

	NetworkManager::RemoveSocket(socketID_);
}

void PlayersAdmin::AddPlayer(int id, Players* pPlayer) {

	pPlayer->SetPlayerId(id);
	pPlayerMap_[id] = pPlayer;
}

void PlayersAdmin::SendData() const {

	PLAYER_DATA data{
		SessionManager::GetInstance()->GetMyPlayerId(),
		myself->IsDemon(),
		myself->GetPosition(),
		myself->GetRotate(),
		myself->GetNextDemonPlayerId()
	};
	myDataPacket_->SetRawData(data);

	NetworkManager::Send(socketID_, myDataPacket_);
}

void PlayersAdmin::ReceiveData() {
	
	//データパケットの数を取得
	auto receiveCount = NetworkManager::GetReceivePacketCount(socketID_);	

	//データパケットの数だけループ
	for (int i = 0; i < receiveCount; i++) {

		//受信データ
		auto receivePacket = NetworkManager::PopReceivePacket(socketID_);

		//PlayerDataPacket型にキャスト(不可ならnullptr)
		auto playerDataPacket = std::dynamic_pointer_cast<PlayerDataPacket>(receivePacket);

		//playerDataPacketがnullptrでなければ
		if (playerDataPacket) {

			//プレイヤーデータを取得
			auto playerData = playerDataPacket->GetRawData();

			//データの反映
			ReflectData(playerData);
		}
	}
}

void PlayersAdmin::ReflectData(const PLAYER_DATA& data) {

	//追加されたプレイヤーの数だけループ
	//for (auto player : pPlayerMap_) {
	//	
	//	auto playerData = player->GetPlayerData();	//プレイヤーデータを取得
	//	
	//	//プレイヤーIDが一致した場合
	//	if (playerData.id == data.id) {
	//		
	//		player->ReflectData(data);	//データの反映
	//		break;
	//	}
	//}

	
	auto result = pPlayerMap_.find(data.id);
	if (result == pPlayerMap_.end()) {

		AddPlayer(data.id, Instantiate<Enemy>(this));
	}
	
	if (data.nextDemonPlayerId == myself->GetPlayerId()) {

		myself->SetDemon(true);
	}

	pPlayerMap_.at(data.id)->ReflectData(data);
}
