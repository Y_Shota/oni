#pragma once
#include "Socket.h"

#include<list>

class TcpServerSocket : public Socket {

public:
	void Open(u_short port) override;
	void Connect(const char* destination) override;
	void Close() override;
	void SetBlocking(bool isBlocking) const override;

protected:
	void ReceiveAll() override;
	void SendAll() override;

private:
	SOCKET sock0_;
	std::list<SOCKET> socketList_;

};

