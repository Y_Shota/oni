#pragma once
#include <exception>
#include <string>

/**
* \brief ネットワークに関わる例外
*/
class NetworkException final : public std::exception {

public:
	explicit NetworkException(const std::string& msg) : std::exception(msg.c_str()) {}
	explicit NetworkException(const char* const msg) : std::exception(msg) {}
	NetworkException(const char* const msg, const int errorCode) : NetworkException(msg + std::to_string(errorCode)) {}
	NetworkException(const std::string& msg, const int errorCode) : NetworkException(msg + std::to_string(errorCode)) {}
};
