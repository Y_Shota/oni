#include "BinaryPacket.h"

using namespace std;


BinaryPacket::BinaryPacket()
	: size_(0) {}

BinaryPacket::~BinaryPacket() = default;

void BinaryPacket::SetData(const char* data, u_long size) {

	data_ = make_unique<char[]>(size);
	size_ = size;
	memcpy_s(data_.get(), size, data, size);
}

auto BinaryPacket::GetSize() const -> u_long {

	return size_;
}

auto BinaryPacket::GetData() const -> const char* {

	return data_.get();
}
