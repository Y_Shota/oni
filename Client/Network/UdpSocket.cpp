#include "UdpSocket.h"

#include <array>


#include "BinaryPacket.h"
#include "IPacket.h"
#include "NetworkException.h"
#include "NetworkManager.h"

using namespace std;



UdpSocket::UdpSocket()
	: localSocket_(INVALID_SOCKET), localPort_(0)
	, remoteSocket_(INVALID_SOCKET), remotePort_(0) {}

void UdpSocket::ReceiveAll() {

	if (localSocket_ == INVALID_SOCKET)	return;
	
	while(true) {

		array<char, 256> buffer;
		sockaddr_in fromAddress;
		auto fromLen = static_cast<int>(sizeof(fromAddress));
		
		auto result = recvfrom(
			localSocket_,
			buffer.data(), static_cast<int>(buffer.size()),
			0,
			reinterpret_cast<sockaddr*>(&fromAddress), &fromLen);

		if (result == SOCKET_ERROR) {
			
			auto errorCode = WSAGetLastError();
			if (errorCode == WSAEWOULDBLOCK) break;

			throw NetworkException("recvfrom() : ErrorCode ", errorCode);
		}

		shared_ptr<IPacket> packet;
		if (isBinary_) {

			packet = std::make_shared<BinaryPacket>();
		}
		else {

			auto id = ntohl(*(reinterpret_cast<int*>(buffer.data())));
			packet = NetworkManager::CreateIdPacket(id);
		}
		packet->SetData(buffer.data(), result);
		inList_.emplace_back(packet);

		recipientSet_.emplace(fromAddress.sin_addr.s_addr);
	}
}

void UdpSocket::SendAll() {

	if (remoteSocket_ == INVALID_SOCKET)	return;
	if (outList_.empty())					return;
	
	while(!outList_.empty()) {
		
		if (destinationSet_.empty()) {

			outList_.clear();
			break;
		}

		const auto& packet = outList_.front();
		
		for (const auto& destination : destinationSet_) {

			sockaddr_in toAddress;
			toAddress.sin_family = AF_INET;
			toAddress.sin_port = htons(remotePort_);
			toAddress.sin_addr.s_addr = destination;
			auto toLen  = static_cast<int>(sizeof(toAddress));
			
			auto result = sendto(
				remoteSocket_,
				packet->GetData(), packet->GetSize(),
				0,
				reinterpret_cast<sockaddr*>(&toAddress), toLen);

			if (result == SOCKET_ERROR) throw NetworkException("sendto() : ErrorCode ", WSAGetLastError());
		}
		outList_.pop_front();
	}
}

void UdpSocket::Close() {

	if (localSocket_ != INVALID_SOCKET) {

		closesocket(localSocket_);
		localSocket_ = INVALID_SOCKET;
	}
	
	if (remoteSocket_ != INVALID_SOCKET) {

		closesocket(remoteSocket_);
		remoteSocket_ = INVALID_SOCKET;
	}
}

void UdpSocket::SetBlocking(bool isBlocking) const {

	u_long flg = isBlocking ? 0 : 1;
	ioctlsocket(localSocket_, FIONBIO, &flg);
	ioctlsocket(remoteSocket_, FIONBIO, &flg);
}

void UdpSocket::CreateLocalSocket(u_short port) {

	localSocket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (localSocket_ == INVALID_SOCKET)	throw NetworkException("socket() : ErrorCode ", WSAGetLastError());

	sockaddr_in hostAddress{};
	hostAddress.sin_family = AF_INET;
	hostAddress.sin_port = htons(port);
	hostAddress.sin_addr.s_addr = INADDR_ANY;

	auto result = bind(localSocket_, reinterpret_cast<sockaddr*>(&hostAddress), sizeof(hostAddress));
	if (result == SOCKET_ERROR)		throw NetworkException("bind() : ErrorCode ", WSAGetLastError());

	localPort_ = port;
}

void UdpSocket::CreateRemoteSocket(u_short port) {
	
	remoteSocket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (remoteSocket_ == INVALID_SOCKET)	throw NetworkException("socket() : ErrorCode ", WSAGetLastError());

	remotePort_ = port;
}
