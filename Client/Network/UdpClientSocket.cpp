#include "UdpClientSocket.h"

#include <WS2tcpip.h>

#include "NetworkException.h"


void UdpClientSocket::Open(u_short port) {

	CreateLocalSocket(port + 1);
	CreateRemoteSocket(port);
}

void UdpClientSocket::Connect(const char* destination) {

	if (isLock_)	return;

	u_long address;
	if (inet_pton(AF_INET, destination, &address) != 1)	return;
	
	destinationSet_.clear();
	
	destinationSet_.emplace(address);
}

