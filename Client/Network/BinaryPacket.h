#pragma once
#include <memory>

#include "IPacket.h"

class BinaryPacket : public IPacket {

public:
	BinaryPacket();
	~BinaryPacket();
	
	void SetData(const char* data, u_long size) override;
	auto GetSize() const -> u_long override;
	auto GetData() const -> const char* override;

private:
	std::unique_ptr<char[]> data_;
	u_long size_;
};
