#pragma once

#include <list>
#include <memory>
#include <WinSock2.h>

class IPacket;


class Socket {

public:
	Socket();
	virtual ~Socket();

	virtual void Open(u_short port) = 0;
	virtual void Connect(const char* destination) = 0;
	virtual void Close() = 0;

	void Update();
	void Lock();
	void Unlock();

	void Send(std::shared_ptr<IPacket> packet);
	auto FrontReceivePacket() -> std::shared_ptr<IPacket>;
	void PopFrontReceivePacket();

	auto GetReceivePacketCount() -> int;
	
	virtual void SetBlocking(bool isBlocking) const = 0;
	void SetBinaryProtocol(bool isBinary);

protected:
	virtual void ReceiveAll() = 0;
	virtual void SendAll() = 0;
	virtual void OtherProcess() {}
	
	using PacketList = std::list<std::shared_ptr<IPacket>>;
	PacketList outList_;
	PacketList inList_;

	bool isLock_;
	bool isBinary_;
};