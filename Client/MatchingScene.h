#pragma once
#include "Engine/GameObject.h"

class MatchingScene final : public GameObject {
	
public:
	explicit MatchingScene(GameObject* parent);
	
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};

