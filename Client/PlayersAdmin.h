//INCLUDE GUARD
#pragma once

//INCLUDE
#define _WINSOCKAPI_
#include <unordered_map>


#include "PlayerDataPacket.h"
#include "Engine/GameObject.h"
#include "SRPlayerData.h"

class Players;
class You;

//プレイヤーを管理するクラス
class PlayersAdmin : public GameObject {
	
private:
	int socketID_;	//ソケットID
	You* myself;	//操作するインスタンス
	std::unordered_map<int, Players*> pPlayerMap_;	//プレイヤーリスト
	std::shared_ptr<PlayerDataPacket> myDataPacket_;
	
public:
	/**
	 * \brief コンストラクタ(引数あり)
	 * \param parent 
	 */
	explicit PlayersAdmin(GameObject* parent);
	/**
	 * \brief デストラクタ
	 */
	~PlayersAdmin();
		
	/**
	 * \brief 初期化
	 */
	void Initialize()	override;
	/**
	 * \brief 更新
	 */
	void Update()		override;
	/**
	 * \brief 描画
	 */
	void Draw()			override;
	/**
	 * \brief 解放
	 */
	void Release()		override;
	
private:

	/**
	 * \brief プレイヤーをリストに追加
	 * \param id プレイヤID
	 * \param pPlayer 追加するプレイヤーのポインタ
	 */
	void AddPlayer(int id, Players* pPlayer);
	/**
	 * \brief サーバへデータを送信する
	 */
	void SendData() const;
	/**
	 * \brief サーバーからデータを受け取る
	 */
	void ReceiveData();
	/**
	 * \brief 受けとったデータを反映させる
	 * \param data 受け取ったデータ
	 */
	void ReflectData(const PLAYER_DATA& data);
};


