#pragma once
#include "Engine/GameObject.h"


enum OBJ
{
	FLR,
	WLL,
};

//ステージを管理するクラス
class Stage : public GameObject
{
private:
	int hModel_[2];					//モデル番号
	int width_;						//幅
	int depth_;						//奥行
	int** table_;					//フィールド範囲

public:
	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//幅を返す関数
	//引数：なし
	//戻値：width_
	int Getwidth_() { return width_; };

	//奥行を返す関数
	//引数：なし
	//戻値：depth_
	int Getdepth_() { return depth_; };

	//そこは壁か調べる関数
	//引数：座標(x, z)
	//戻値：壁ならTrue、床ならFalse
	bool IsWall(int x, int z) { return table_[x][z]; };
};