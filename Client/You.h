//インクルードガード
#pragma once


//インクルード
#include "Players.h"


//プロトタイプ宣言
class Stage;


//操作キャラ
class You : public Players
{
private:
	//ステージポインタ
	Stage* pStage_;
	
	//ステージを見つける
	void GetStage();

private:
	//カメラ
	float camAng_;
	float dirOnInput_;

	//行動入力
	void Input();

	//プレイヤー移動
	void Move();

	//壁へのめりこみ回避
	void SaveWall(XMVECTOR move);

	//カメラの移動
	void CameraConfig();

private:
	int nextDemonPlayerId_;

public:
	//コンストラクタ
	You(GameObject* parent);

	//デストラクタ
	~You();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//開放
	void Release() override;
	
	void ReflectData(const PLAYER_DATA& data) override{}
	void OnCollision(GameObject* pTarget) override;

	auto GetNextDemonPlayerId() noexcept -> int;
};

