//インクルード
#include "Enemy.h"
#include "Engine/Model.h"


//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:Players(parent, "Enemy")
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	ID_[0] = Model::Load("Object/Character/Enemy.fbx");
	assert(ID_[0] >= 0);

}

//更新
void Enemy::Update()
{
	cantTime_ -= signbit((float)-cantTime_);
}

//開放
void Enemy::Release()
{

}
