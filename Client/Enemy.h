//インクルードガード
#pragma once


//インクルード
#include <DirectXMath.h>
#include "Players.h"



//操作キャラ
class Enemy : public Players
{
public:
	//コンストラクタ
	Enemy(GameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//開放
	void Release() override;
};

