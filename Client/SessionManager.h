#pragma once
#define _WINSOCKAPI_
#include <memory>
#include <optional>

class SessionPacket;

//データを管理するクラス
class SessionManager {
	
private:

	int sessionSocket_;		//メッセージソケット
	bool isConnected_;
	int myPlayerId_;
	bool firstDemon_;
	std::optional<long long> keyNumber_;

public:
	//デストラクタ
	~SessionManager();

	//初期化
	void Initialize();

	//更新
	void Update();

	//開放
	void Release();

	void SendJoinMessage();
	void SendLeaveMessage() const;
	void SendStartMessage() const;
	void SendStartedMessage() const;
	void SendEndedMessage() const;

	auto GetMyPlayerId() const -> int;
	auto IsFirstDemon() const -> bool;

private:
	//コンストラクタ
	SessionManager();

	//コピー、ムーブ禁止
	SessionManager(const SessionManager&) = delete;
	SessionManager& operator=(const SessionManager&) = delete;
	SessionManager(SessionManager&&) = delete;
	SessionManager& operator=(SessionManager&&) = delete;

	//受け取ったデータを適用する
	void ApplySessionPacket(const std::shared_ptr<SessionPacket>& packet);

public:
	static auto GetInstance() -> SessionManager*;
	static void Create();
	static void Destroy();
	
private:
	static std::unique_ptr<SessionManager> instance_;
	
};