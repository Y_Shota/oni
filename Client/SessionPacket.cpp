#include "SessionPacket.h"

PacketId<SessionData>::NetworkType SessionPacket::ConvertToNetwork(HostType& host) {

	NetworkType network;
	network.id = htonl(host.id);
	network.sessionType = htonl(host.sessionType);
	network.option = htonll(host.option);

	return network;
}

PacketId<SessionData>::HostType SessionPacket::ConvertToHost(NetworkType& network) {

	NetworkType host;
	host.id = ntohl(network.id);
	host.sessionType = ntohl(network.sessionType);
	host.option = ntohll(network.option);

	return host;
}
