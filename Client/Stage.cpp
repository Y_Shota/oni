#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Stage.h"


//コンストラクタ
Stage::Stage(GameObject * parent)
	:GameObject(parent, "Stage"),
	width_(0), depth_(0)
{
	//hModel_の初期化
	for (int i = 0; i < 2; i++)
		hModel_[i] = -1;

	//csvの読み込み
	CsvReader csv;
	csv.Load("MapData/MapData.csv");

	//フィールドの縦横読み込み
	width_= (int)csv.GetWidth();		//1行に何個データがあるか
	depth_= (int)csv.GetHeight();		//データが何行あるか

	//table_の動的生成
	table_ = new int*[width_];
	/*table_の2次元目の生成*/
	for (int i = 0; i < width_; i++)
	{
		table_[i] = new int[depth_];
	}

	//table_の初期化
	/*MapDataからフィールドを読み取る*/
	for (int i = 0; i < width_; i++)
	{
		for (int j = 0; j < depth_; j++)
		{
			table_[i][j] = csv.GetValue(i, j);
		}
	}
}

//デストラクタ
Stage::~Stage()
{
	//table_の削除
	/*2次元目の削除*/
	for (int i = 0; i < width_; i++)
	{
		delete[] table_[i];
	}
	delete[] table_;
}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード
	hModel_[FLR] = Model::Load("Object/Field/Floor.fbx");
	assert(hModel_[FLR] >= 0);
	hModel_[WLL] = Model::Load("Object/Field/Wall.fbx");
	assert(hModel_[WLL] >= 0);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	/*フィールドの描画*/
	for (int x = 0; x < width_; x++)
	{
		for (int z = 0; z < depth_; z++)
		{
			//床or壁判定
			int type = table_[x][z];

			//生成位置調整
			Transform Trans_;
			Trans_.position_ = XMVectorSet((float)x, 0, (float)z, 0);
			Trans_.scale_.vecY = 0.5f;
			
			//モデルの描画
			Model::SetTransform(hModel_[type], Trans_);
			Model::Draw(hModel_[type]);
		}
	}
}

//開放
void Stage::Release()
{
}