#include "MatchingScene.h"

#include "SessionManager.h"
#include "Engine/Input.h"

MatchingScene::MatchingScene(GameObject* parent)
	: GameObject(parent, "MatchingScene") {}

void MatchingScene::Initialize() {

	SessionManager::Create();
}

void MatchingScene::Update() {

	auto* sessionManager = SessionManager::GetInstance();
	sessionManager->Update();

	using namespace Input;
	
	if (IsKeyDown(DIK_RETURN)) {

		sessionManager->SendJoinMessage();
	}
	if (IsKeyDown(DIK_SPACE)) {

		sessionManager->SendStartMessage();
	}
	if (IsKeyDown(DIK_ESCAPE)) {

		sessionManager->SendLeaveMessage();
	}
}

void MatchingScene::Draw() {}

void MatchingScene::Release() {}
