//インクルード
#include "Players.h"
#include "Engine/Model.h"
#include "Stage.h"


static const int MAX_TIME = 60;

//コンストラクタ
Players::Players(GameObject* parent, const std::string& name)
	:GameObject(parent, "Players")
{
	//モデルID
	ID_[0] = INIT_ID;
	ID_[1] = Model::Load("Object/Character/Oni.fbx");

	//鬼かどうか
	isOni_ = false;

	//鬼が移った時のタイム
	cantTime_ = 0;

	playerId_ = -1;
	
	AddCollider(new SphereCollider(XMVectorSet(0, 0.4f, 0, 0), 0.6f));
}

//デストラクタ
Players::~Players()
{
}

//描画
void Players::Draw()
{
	//モデルの描画
	Model::SetTransform(ID_[isOni_], transform_);
	Model::Draw(ID_[isOni_]);
}

//オブジェクトを固定
void Players::SetTransform(float x, float y, float z, float angY)
{
	//固定するデータ
	Transform trans;

	//値設定
	trans.position_ = XMVectorSet(x, y, z, 0);
	trans.rotate_.vecY = angY;

	//固定
	SetTransform(trans);
}

//鬼状態の設定
void Players::SetDemon(bool isOni)
{
	//鬼状態変更
	isOni_ = isOni;

	//鬼になった
	cantTime_ = MAX_TIME * isOni_;
}

auto Players::IsDemon() const -> bool {

	return isOni_;
}

//データを反映させる
void Players::ReflectData(const PLAYER_DATA& data) {

	transform_.position_ = data.position;
	transform_.rotate_ = data.rotate;

	SetDemon(data.isDemon);
}

auto Players::CanChangeDemon() const -> bool {

	return cantTime_ <= 0;
}

void Players::SetPlayerId(int id) {

	playerId_ = id;
}

auto Players::GetPlayerId() const noexcept -> int {

	return playerId_;
}
