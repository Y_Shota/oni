#include "sceneManager.h"
#include "../PlayScene.h"
#include "../TestScene.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"
#include "Time.h"
#include "../MatchingScene.h"

SceneManager* SceneManager::instanceRef_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager")
	, currentSceneID_(SCENE_ID_UNKNOWN), nextSceneID_(SCENE_ID_UNKNOWN) {}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_MATCH;
	nextSceneID_ = currentSceneID_;
	Instantiate<MatchingScene>(this);

	instanceRef_ = this;
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Audio::AllRelease();
		Model::AllRelease();
		Image::AllRelease();

		//次のシーンを作成
		switch (nextSceneID_)
		{
		case SCENE_ID_TEST: Instantiate<TestScene>(this); break;
		case SCENE_ID_PLAY: Instantiate<PlayScene>(this); break;
		case SCENE_ID_MATCH: Instantiate<MatchingScene>(this); break;
		default: break;
		}

		currentSceneID_ = nextSceneID_;

		Time::ResetSceneTimer();
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}

auto SceneManager::GetInstance() -> SceneManager* {

	return instanceRef_;
}
