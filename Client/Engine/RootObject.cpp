#define _WINSOCKAPI_
#include "RootObject.h"
#include "SceneManager.h"
#include "../Network/NetworkManager.h"

RootObject::RootObject():
	GameObject(nullptr, "RootObject")
{
}


RootObject::~RootObject()
{
}

void RootObject::Initialize()
{
	NetworkManager::Initialize();
	Instantiate<SceneManager>(this);
}

void RootObject::Update()
{
	NetworkManager::Update();
}

void RootObject::Draw()
{
}

void RootObject::Release()
{
	NetworkManager::Finalize();
}
