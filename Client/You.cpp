//インクルード
#include "You.h"

#include "Enemy.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"


//名前空間
using namespace Input;


//定数
//Initialize()
static const float ADJ_INIT_POS = 0.5f;							//PLの初期生成位置の調整
static const float ADJ_CENTER = 2.0f;							//ステージの中心ベクトルを指す際に使用
static const XMVECTOR CAM_POS = XMVectorSet(0, 2, -5, 0);		//カメラの位置
static const XMVECTOR CAM_TAR = XMVectorSet(0, 0, 5, 0);		//カメラの焦点

//Input()
static const float NOR_SPD = 0.2f;								//鬼状態に関係ない基礎移動速度
static const float ONI_SPD = 0.02f;								//鬼状態の時に加算される移動速度
static const float SLOWLY_SPD = 0.5f;							//ゆっくり移動する際の移動距離

//Initialize(), Input()
static const float ADJ_ANG = 0.1f;								//外積を出す際に180度をしっかり見極める

//Input(), CameraConfig()
static const float BACK_ANG = 180.0f;							//180度

//SaveWall()
static const float PL_SIZE = 0.3f;		//PLのモデルの半径


//コンストラクタ
You::You(GameObject * parent)
	:Players(parent, "You")
	, nextDemonPlayerId_(-1)
{
	
}

//デストラクタ
You::~You()
{
}

//初期化
void You::Initialize()
{
	//モデルデータのロード
	ID_[0] = Model::Load("Object/Character/Player.fbx");
	assert(ID_[0] >= 0);
	
	//ステージ
	pStage_ = nullptr;

	//ステージを見つける
	GetStage();

	//初期位置
	while (true)
	{
		//ブロック以外のところ指定
		int x = rand() % pStage_->Getwidth_();
		int z = rand() % pStage_->Getdepth_();

		//ブロック
		if (pStage_->IsWall(x, z))continue;

		//初期位置設定
		transform_.position_.vecX = x + ADJ_INIT_POS;
		transform_.position_.vecZ = z + ADJ_INIT_POS;

		//終了
		break;
	}

	//カメラの位置とターゲット
	camAng_ = 0;
	dirOnInput_ = 0;

	/*プレイヤーの向きをフィールドの中心に寄せる*/
	//フィールドの中心を指すベクトル
	XMVECTOR field = XMVectorSet((float)pStage_->Getwidth_(), 0, (float)pStage_->Getdepth_(), 0) / ADJ_CENTER;

	//自身とフィールドの中心との差を取る
	XMVECTOR diff = field - transform_.position_;

	//自身と差との角度を取得
	float angle = XMConvertToDegrees((XMVector3AngleBetweenVectors(g_XMIdentityR2, diff).vecY));

	//外積を求める際に真後ろの入力のみ180度=上方向が解になることを防ぐ
	diff.vecX += ADJ_ANG;
	
	//角度を外積によって"+-"判定する
	angle *= XMVector3Normalize(XMVector3Cross(g_XMIdentityR2, XMVector3Normalize(diff))).vecY;

	//角度変換
	transform_.rotate_.vecY = angle;

	//カメラ方向設定
	camAng_ = transform_.rotate_.vecY;
}

//更新
void You::Update()
{
	//行動するか
	cantTime_ -= signbit((float)-cantTime_);
	if (cantTime_ > 0) return;

	//行動入力
	Input();

	//テスト処理
	isOni_ = isOni_ ^ IsKeyDown(DIK_F);
}

//開放
void You::Release()
{
	//ステージ開放
	pStage_ = nullptr;
}

void You::OnCollision(GameObject* pTarget) {

	if (auto* enemy = dynamic_cast<Enemy*>(pTarget)) {

		if (IsDemon() && CanChangeDemon()) {

			SetDemon(false);
			nextDemonPlayerId_ = enemy->GetPlayerId();
		}
	}
}

//ステージを見つける
void You::GetStage()
{
	//ステージオブジェクトを探す
	pStage_ = (Stage*)FindObject("Stage");
	assert(pStage_ != nullptr);
}

//行動入力
void You::Input()
{
	//プレイヤー移動
	Move();

	//カメラの移動
	CameraConfig();
}

//プレイヤー移動
void You::Move()
{
	/*入力角度計算*/
	//入力ベクトル(入力方向)
	XMVECTOR input = XMVectorSet(CalcKey(DIK_D, DIK_A), 0, CalcKey(DIK_W, DIK_S), 0);

	//入力なし
	if (!XMVector3Length(input).vecX)return;

	//角度計算
	float angle = XMVector3AngleBetweenVectors(g_XMIdentityR2, input).vecY;

	//外積を求める際に真後ろの入力のみ180度=上方向が解になることを防ぐ
	input.vecX += ADJ_ANG;

	//角度を外積によって"+-"判定する
	angle *= XMVector3Normalize(XMVector3Cross(g_XMIdentityR2, XMVector3Normalize(input))).vecY;


	/*移動*/
	//自身の角度変更(カメラの角度 + 入力方向)
	transform_.rotate_.vecY = camAng_ + XMConvertToDegrees(angle);

	//移動ベクトル
	XMVECTOR move = g_XMIdentityR2;

	//moveを自身の角度分回転
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, matY);

	//スピード
	float spd = NOR_SPD;

	//鬼の場合、スピードが速くなる
	spd += isOni_ * ONI_SPD;

	//シフト入力時は、スピードが遅くなる
	spd -= spd * IsKey(DIK_LSHIFT) * SLOWLY_SPD;

	//移動量計算
	move *= spd;
	
	//壁へのめりこみ回避
	SaveWall(move);
}

//壁へのめりこみ回避
void You::SaveWall(XMVECTOR move)
{
	//めり込み具合取得
	float intoX, intoZ;

	//移動先座標
	XMVECTOR nextPos = transform_.position_ + move;

	/*
	<判定式>
	　int	wall  =  衝突判定を行う壁の辺
	  float pos	  =  衝突判定を行うPLの位置(半径が足されるか引かれる)
	  float diff  =  wall - pos
	  bool isWall =  wallの位置は実際に壁かどうか
	  float into  =  diff * mag(壁じゃない場合は0になる)
	*/

	//右判定(Right)
	int wallR = (int)(nextPos.vecX + PL_SIZE);
	float posR = nextPos.vecX + PL_SIZE;
	float diffR = wallR - posR;
	bool isWallR = pStage_->IsWall((int)(nextPos.vecX + PL_SIZE), (int)nextPos.vecZ);
	intoX = diffR * isWallR;

	//左判定(Left)
	int wallL = (int)(nextPos.vecX);
	float posL = nextPos.vecX - PL_SIZE;
	float diffL = wallL - posL;
	bool isWallL = pStage_->IsWall((int)(nextPos.vecX - PL_SIZE), (int)nextPos.vecZ);
	intoX += diffL * isWallL;

	//奥判定(Back)
	int wallB = (int)(nextPos.vecZ + PL_SIZE);
	float posB = nextPos.vecZ + PL_SIZE;
	float diffB = wallB - posB;
	bool isWallB = pStage_->IsWall((int)nextPos.vecX, (int)(nextPos.vecZ + PL_SIZE));
	intoZ = diffB * isWallB;

	//手前判定(Front)
	int wallF = (int)(nextPos.vecZ);
	float posF = nextPos.vecZ - PL_SIZE;
	float diffF = wallF - posF;
	bool isWallF = pStage_->IsWall((int)nextPos.vecX, (int)(nextPos.vecZ - PL_SIZE));
	intoZ += diffF * isWallF;

	//めり込みを戻す
	move.vecX += intoX;
	move.vecZ += intoZ;

	//移動(壁にめり込んだ際の挙動を回避)
	transform_.position_ += move * !((isWallR & isWallB) | (isWallR & isWallF) | (isWallL & isWallB) | (isWallL & isWallF));
}

//カメラの移動
void You::CameraConfig()
{
	/*カメラ入力*/
	//カメラ回転(カメラが逆向きになっていないとき)
	camAng_ += CalcKey(DIK_RIGHT, DIK_LEFT) * !IsKey(DIK_V) * 2;

	//スペース入力でカメラを更に回転(カメラが逆向きになっていないとき)
	camAng_ += CalcKey(DIK_RIGHT, DIK_LEFT) * IsKey(DIK_SPACE) * !IsKey(DIK_V);

	//コントロールを押していないときはカメラの向き
	camAng_ = camAng_ * !IsKeyDown(DIK_LCONTROL) * !IsKeyDown(DIK_Q);

	//コントロールを押したらプレイヤーの向き
	camAng_ += transform_.rotate_.vecY * IsKeyDown(DIK_LCONTROL) * !IsKeyDown(DIK_Q);

	//Qが押された場合カメラの向きをプレイヤーを前から見たカメラの向きにする
	camAng_ += (transform_.rotate_.vecY + BACK_ANG) * IsKeyDown(DIK_Q);


	/*カメラ操作*/
	//カメラの位置とターゲット
	XMVECTOR camPos = CAM_POS;
	XMVECTOR camTar = CAM_TAR;

	//既にVキーを押しているまたは未入力時には向きを記憶しない
	dirOnInput_ = dirOnInput_ * !IsKeyDown(DIK_V);

	//Vキーをこのフレームで入力時の向きを記憶
	dirOnInput_ += (transform_.rotate_.vecY + BACK_ANG) * IsKeyDown(DIK_V);

	//カメラ回転用
	XMMATRIX matY;

	//Vキーを押した時点でのプレイヤーを前から見たカメラワーク
	matY = XMMatrixRotationY(XMConvertToRadians(dirOnInput_)) * IsKey(DIK_V);

	//カメラ中心のカメラワーク
	matY += XMMatrixRotationY(XMConvertToRadians(camAng_)) * !IsKey(DIK_V);

	//カメラ回転
	camPos = XMVector3TransformCoord(camPos, matY);
	camTar = XMVector3TransformCoord(camTar, matY);

	//カメラ変更
	Camera::SetPosition(camPos + transform_.position_);
	Camera::SetTarget(camTar + transform_.position_);
}

auto You::GetNextDemonPlayerId() noexcept -> int {

	auto temp = nextDemonPlayerId_;
	nextDemonPlayerId_ = -1;
	return temp;
}

/*
<input()>
if(コントロール)
回転角度(A) = 360 + (外積 * 内積で求めた角度)
A -= A % 4;

<update()>
if(A > 0)
{
	A-=4;
	transform_.rotate_.vecY -= 4;
}
*/